package cz.cvut.fit.prachtom.service;

import cz.cvut.fit.prachtom.dao.ContactDao;
import cz.cvut.fit.prachtom.model.Contact;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;

@Service
public class ContactService {
    @Autowired
    private ContactDao contactDao;

    @Transactional
    public void add(Contact contact) {
        contactDao.persist(contact);
    }

    @Transactional
    public void addAll(Collection<Contact> contacts) {
        for (Contact contact : contacts) {
            contactDao.persist(contact);
        }
    }

    @Transactional(readOnly = true)
    public List<Contact> listAll() {
        return contactDao.findAll();
    }
}
