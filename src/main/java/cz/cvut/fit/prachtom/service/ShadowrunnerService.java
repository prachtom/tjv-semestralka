package cz.cvut.fit.prachtom.service;

import cz.cvut.fit.prachtom.dao.ShadowrunnerDao;
import cz.cvut.fit.prachtom.model.Shadowrunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;

@Service
public class ShadowrunnerService {
    @Autowired
    private ShadowrunnerDao shadowrunnerDao;

    @Transactional
    public void add(Shadowrunner shadowrunner) {
        shadowrunnerDao.persist(shadowrunner);
    }

    @Transactional
    public void addAll(Collection<Shadowrunner> shadowrunners) {
        for (Shadowrunner shadowrunner : shadowrunners) {
            shadowrunnerDao.persist(shadowrunner);
        }
    }

    @Transactional(readOnly = true)
    public List<Shadowrunner> listAll() {
        return shadowrunnerDao.findAll();
    }

}
