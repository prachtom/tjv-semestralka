package cz.cvut.fit.prachtom.service;

import cz.cvut.fit.prachtom.dao.RunDao;
import cz.cvut.fit.prachtom.model.Run;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;

@Service
public class RunService {
    @Autowired
    private RunDao runDao;

    @Transactional
    public void add(Run run) {
        runDao.persist(run);
    }

    @Transactional
    public void addAll(Collection<Run> runs) {
        for (Run run : runs) {
            runDao.persist(run);
        }
    }

    @Transactional(readOnly = true)
    public List<Run> listAll() {
        return runDao.findAll();
    }

}
