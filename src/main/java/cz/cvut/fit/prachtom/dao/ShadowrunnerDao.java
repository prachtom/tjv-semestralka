package cz.cvut.fit.prachtom.dao;

import cz.cvut.fit.prachtom.model.Shadowrunner;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class ShadowrunnerDao {

    @PersistenceContext
    private EntityManager em;

    public void persist(Shadowrunner shadowrunner) {
        em.persist(shadowrunner);
    }

    public List<Shadowrunner> findAll() {
        return em.createQuery("SELECT r FROM Shadowrunner r", Shadowrunner.class).getResultList();
    }

}
