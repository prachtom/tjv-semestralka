package cz.cvut.fit.prachtom.dao;

import cz.cvut.fit.prachtom.model.Contact;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class ContactDao {

    @PersistenceContext
    private EntityManager em;

    public void persist(Contact contact) {
        em.persist(contact);
    }

    public List<Contact> findAll() {
        return em.createQuery("SELECT c FROM Contact c", Contact.class).getResultList();
    }

}
