package cz.cvut.fit.prachtom.dao;

import cz.cvut.fit.prachtom.model.Run;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class RunDao {

    @PersistenceContext
    private EntityManager em;

    public void persist(Run run) {
        em.persist(run);
    }

    public List<Run> findAll() {
        return em.createQuery("SELECT r FROM Run r", Run.class).getResultList();
    }

}
