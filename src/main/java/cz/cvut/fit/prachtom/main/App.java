package cz.cvut.fit.prachtom.main;

import cz.cvut.fit.prachtom.model.Contact;
import cz.cvut.fit.prachtom.model.Shadowrunner;
import cz.cvut.fit.prachtom.model.Run;
import cz.cvut.fit.prachtom.service.ContactService;
import cz.cvut.fit.prachtom.service.RunService;
import cz.cvut.fit.prachtom.service.ShadowrunnerService;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.dao.DataAccessException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class App {

    public static void main(String[] args)
    {
        //System.out.println("yes");



        //Create Spring application context
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("spring.xml");

        //Get service from context. (service's dependency (RunDAO) is autowired in RunService)
        ContactService contactService = ctx.getBean(ContactService.class);
        ShadowrunnerService shadowrunnerService = ctx.getBean(ShadowrunnerService.class);
        RunService runService = ctx.getBean(RunService.class);



        //Do some data operation

        Contact janNovak = new Contact("Jan Novak");
        Contact josefMaly = new Contact("Josef Maly");
        Contact petrCerny = new Contact("Petr Cerny");


        contactService.add(janNovak);
        contactService.add(petrCerny);
        //System.out.println("listAll contacts: " + contactService.listAll());
        contactService.add(josefMaly);
        //System.out.println("listAll contacts: " + contactService.listAll());

        Run run1 = new Run("kill me", janNovak);
        Run run2 = new Run("steal me", janNovak);
        Run run3 = new Run("save me", janNovak);
        runService.add(run1);
        runService.add(run2);
        runService.add(run3);




        Shadowrunner techno = new Shadowrunner("techno","decker",petrCerny);
        Shadowrunner ruben = new Shadowrunner("ruben","adept",josefMaly);
        shadowrunnerService.add(techno);
        shadowrunnerService.add(ruben);

        run1.getParticipants().add(techno);
        run1.getParticipants().add(ruben);
        ruben.getRuns().add(run1);

        runService.add(run1);
        shadowrunnerService.add(ruben);

        System.out.println("C: " + contactService.listAll().size() + "; R: " + runService.listAll().size() + "; S: " + shadowrunnerService.listAll().size());




        //System.out.println("listAll shadowrunners: " + shadowrunnerService.listAll());


        //List<Run> runList = new ArrayList<>();
        //runList.add(run1);
        //runList.add(run2);
        //runList.add(run3);

        //runService.addAll(runList);

        //System.out.println("listAll contacts: " + contactService.listAll());

        //System.out.println("listAll: " + runService.listAll());

        //Test transaction rollback (duplicated key)

        /*
        try {
            runService.addAll(Arrays.asList(new Run("Book me", petrCerny), new Run("Soap me", petrCerny), new Run("Compute me", petrCerny)));
        } catch (DataAccessException dataAccessException) {
        }
        */

        //Test element list after rollback
        //System.out.println("listAll: " + runService.listAll());

        ctx.close();

    }
}
