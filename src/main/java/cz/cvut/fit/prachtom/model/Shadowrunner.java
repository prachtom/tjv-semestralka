package cz.cvut.fit.prachtom.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Shadowrunner {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String nickname;

    private String specialization;

    @OneToOne
    private Contact identity;

    //@ManyToMany(fetch = FetchType.EAGER, mappedBy = "participants")*/
/*
 */
    @ManyToMany(mappedBy = "participants")
    private List<Run> runs = new ArrayList<Run>();

    public Shadowrunner() {
    }

    public Shadowrunner(String nickname, String specialization,Contact identity) {
        this.nickname = nickname;
        this.specialization = specialization;
        this.identity = identity;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public Contact getIdentity() {
        return identity;
    }

    public void setIdentity(Contact identity) {
        this.identity = identity;
    }

    public List<Run> getRuns() {
        return runs;
    }

    public void setRuns(List<Run> runs) {
        this.runs = runs;
    }
/*
/*
    @Override
    public String toString() {
        return "id=" + id + ", nickname=" + nickname + ", specialization=" + specialization + ", realname=" + identity.getName() + ", jobcount=" + runs.size();
    }
    */
}
