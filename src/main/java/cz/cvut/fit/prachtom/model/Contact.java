package cz.cvut.fit.prachtom.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Contact {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;

    @OneToMany(fetch = FetchType.EAGER, mappedBy="submitter")
    private List<Run> jobs = new ArrayList<Run>();

    public Contact() {
    }

    public Contact(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Run> getJobs() {
        return jobs;
    }

    public void setJobs(List<Run> jobs) {
       this.jobs = jobs;
    }

    private String listJobs()
    {
        String s = "{";
        for (Run r: jobs) {
            s+=r.getDescription()+ " ";
        }
        return s + "}";
    }
/*
    @Override
    public String toString() {
        return "{id=" + id + ", name=" + name + ", jobs=" +listJobs() + "}"; // + jobs.size();
    }
    */
}
