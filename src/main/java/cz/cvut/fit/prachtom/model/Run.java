package cz.cvut.fit.prachtom.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Run {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String description;

    @ManyToOne
    private Contact submitter;

    /*@ManyToMany(fetch = FetchType.EAGER, cascade = {
            CascadeType.PERSIST,
            CascadeType.MERGE
    })
    @JoinTable(name = "run_shadowrunner",
            joinColumns = @JoinColumn(name = "run_id"),
            inverseJoinColumns = @JoinColumn(name = "shadowrunner_id"))
            (cascade = CascadeType.MERGE)
            */
//    @JoinTable(name = "run_shadowrunner",
//            joinColumns = @JoinColumn(name = "run_id"),
//            inverseJoinColumns = @JoinColumn(name = "shadowrunner_id"))

    @ManyToMany
    private List<Shadowrunner> participants = new ArrayList<>();

    public Run() {
    }

    public Run(String description, Contact submitter) {
        this.description = description;
        this.submitter = submitter;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Contact getSubmitter() {
        return submitter;
    }

    public void setSubmitter(Contact submitter) {
        this.submitter = submitter;
    }

    public List<Shadowrunner> getParticipants() {
        return participants;
    }

    public void setParticipants(List<Shadowrunner> participants) {
        this.participants = participants;
    }
/*
/*
    @Override
    public String toString() {
        return "id=" + id + ", description=" + description + ", submitter=" + submitter.getName() + ", runners=" + participants.size();
    }
    */
}
